from django.conf.urls import url
from django.contrib.auth import views as auth_views
from rest_framework.authtoken import views as rest_framework_views
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

app_name = 'api'

urlpatterns = [   
    #User Login Rest API
    url(r'^user-login/$', views.UserLoginAPIView.as_view()),
    url(r'^create-profile/$', views.CreateProfile.as_view()),
    url(r'^reset-password', views.ResetPasswordRequestView.as_view(),
        name="reset_password"),
]

urlpatterns = format_suffix_patterns(urlpatterns)
