from django.contrib.auth.models import User
from rest_framework import serializers

class UserLoginSerializer(serializers.Serializer):
    class Meta:
        model = User
        fields = [
            'username',
            'password',    
        ]
        extra_kwargs = {"password":
                            {"write_only": True}
                            }

class UserCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name','email', 'password')

    def create(self, validated_data):
        user = super(UserCreateSerializer, self).create(validated_data)
        user.username = validated_data['email']
        user.set_password(validated_data['password'])
        user.save()
        return user
