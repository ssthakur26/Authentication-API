''' Import Modules'''
from django.conf import settings
#import gpxpy.gpx
#import gpxpy.parser as parser
from django.contrib import messages
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.auth.tokens import default_token_generator
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.core.validators import validate_email
from django.db.models.query_utils import Q
from django.shortcuts import redirect, render
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
#from drf_multiple_model.views import ObjectMultipleModelAPIView
from rest_framework import authentication, generics, permissions, viewsets
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from django.template import loader
from rest_framework.parsers import FormParser, JSONParser, MultiPartParser
from rest_framework.permissions import AllowAny
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.status import (HTTP_200_OK, HTTP_201_CREATED,
                                   HTTP_400_BAD_REQUEST)
from rest_framework.views import APIView

from .serializers import UserCreateSerializer, UserLoginSerializer


'''
authentication_classes = (authentication.TokenAuthentication,)
permission_classes = (permissions.IsAuthenticated,) 
'''
# Create your views here.
class UserLoginAPIView(APIView):
    permission_classes = [AllowAny]
    serializer_class = UserLoginSerializer
    renderer_classes = (JSONRenderer, )
    
    def post(self, request, *args, **kwargs):
        data = request.data
        username = data.get('username',None)
        password = data.get('password', None) 
        user = authenticate(username=username, password=password)
        if user is not None:
            # the password verified for the user
            if user.is_active:
                token, created = Token.objects.get_or_create(user=user)
                if token:
                    response = {
                        "message":"This User is successfully authenticated",
                    }
                    return Response(response, status=HTTP_200_OK)
                else:
                    return Response(created, status=HTTP_400_BAD_REQUEST)
        else:
            errors = {
                'data': "This User doesn't exist."
            }
            return Response(errors, status=HTTP_400_BAD_REQUEST)

class CreateProfile(APIView):
    permission_classes = [AllowAny]
    serializer_class = UserLoginSerializer
    renderer_classes = (JSONRenderer, )

    def post(self, request, *args,**kwargs):
        data = request.data
        if data:
            create_seriliazer = UserCreateSerializer(data=data)
            if create_seriliazer.is_valid(raise_exception=True):
                existing = User.objects.filter(username=data.get('email'))
                if existing.count() == 0:
                    create_seriliazer.save()
                    msg = {
                        'created':"{0} Created successfullly".format(request.data.get('first_name'))
                    }
                    return Response(msg, status=HTTP_201_CREATED)
                else:
                    msg = {
                        'created':"This user already exists",
                    }
                    return Response(msg, status=HTTP_201_CREATED)
        else:
            msg = {
                    'unsuccessfull':"Please provide valid parameters",
                }
            return Response(msg, status=HTTP_201_CREATED)

class ResetPasswordRequestView(APIView):
    # code for template is given below the view's code
    permission_classes = [AllowAny]
    serializer_class = UserLoginSerializer
    renderer_classes = (JSONRenderer, )

    @staticmethod
    def validate_email_address(email):

        try:
            validate_email(email)
            return True
        except ValidationError:
            return False

    def reset_password(self, user, request):
        c = {
            'email': user.email,
            'domain': request.META['HTTP_HOST'],
            'site_name': 'your site',
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'user': user,
            'token': default_token_generator.make_token(user),
            'protocol': 'http',
        }
        subject_template_name = 'registration/password_reset_subject.txt'
      
        email_template_name = 'registration/passwordreset_email.html'
       
        subject = loader.render_to_string(subject_template_name, c)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        email = loader.render_to_string(email_template_name, c)
        #send_mail(subject, email, settings.EMAIL_HOST_USER,
        #           [user.email], fail_silently=False)
    def post(self, request, *args, **kwargs):
        data = request.data
        try:
            data = data.get("email")
            # uses the method written above
            if self.validate_email_address(data) is True:
                '''
                If the input is an valid email address, 
                then the following code will lookup for users associated with that email address.
                If found then an email will be sent to the address, 
                else an error message will be printed on the screen.
                '''
                associated_users = User.objects.filter(
                    Q(email=data) | Q(username=data))
                if associated_users.exists():
                    for user in associated_users:
                        self.reset_password(user, request)
                        success = {
                            'An email has been sent to {0}. Please check its inbox to continue reseting password.'.format(data),
                        }
                    return Response(success, status=HTTP_200_OK)
                else:
                    error = {
                            'No user with this email exists',
                    }
                    return Response(error, status=HTTP_400_BAD_REQUEST)
        except Exception as e:
            print(e)
            return Response(e, status=HTTP_400_BAD_REQUEST)
