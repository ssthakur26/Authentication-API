from django.conf.urls import url
from django.contrib.auth import views as auth_views
from .import views
app_name = 'account'

urlpatterns = [
    url(r'^reset_password_confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
        views.PasswordResetConfirmView.as_view(), name='reset_password_confirm'),
    # PS: url above is going to used for next section of
    # implementation.
    url(r'^password_reset/complete/$',
        views.PasswordResetCompleteView.as_view(), name="reset_complete"),
]
