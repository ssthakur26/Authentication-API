''' important modules import '''
from django.contrib import messages
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth import (REDIRECT_FIELD_NAME, get_user_model,
                                 update_session_auth_hash)
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm
from django.contrib.auth.hashers import make_password
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.tokens import default_token_generator
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse_lazy
from django.core.validators import validate_email
from django.db.models.query_utils import Q
from django.shortcuts import redirect, render
from django.template import loader
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.views.generic import (FormView, RedirectView, TemplateView,
                                  UpdateView)

from django.conf import settings

from .forms import PasswordResetRequestForm, SetPasswordForm, UserForm


class PasswordResetConfirmView(FormView):
    template_name = "account/forgot_password.html"
    success_url = reverse_lazy('account:reset_complete')
    form_class = SetPasswordForm
    title         = 'Enter your new password please!!'

    def get(self, request, *args, **kwargs):
        form      = self.form_class(None)
        return render(request, self.template_name, {'form': form, 'title': self.title})

    def post(self, request, uidb64=None, token=None, *arg, **kwargs):
        """
        View that checks the hash in a password reset link and presents a
        form for entering a new password.
        """
        UserModel = get_user_model()
        form = self.form_class(request.POST)
        assert uidb64 is not None and token is not None  # checked by URLconf
        try:
            uid = urlsafe_base64_decode(uidb64)
            user = UserModel._default_manager.get(pk=uid)
        except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
            user = None

        if user is not None and default_token_generator.check_token(user, token):
            if form.is_valid():
                new_password = form.cleaned_data['new_password2']
                user.set_password(new_password)
                user.save()
                messages.success(request, 'Password has been reset.')
                return self.form_valid(form)
            else:
                messages.error(
                    request, 'Password reset has been unsuccessful.')
                return self.form_invalid(form)
        else:
            messages.error(
                request, 'The reset password link is no longer valid.')
            return self.form_invalid(form)


class PasswordResetCompleteView(TemplateView):
    template_name = 'registration/reset_complete.html'

    def get_context_data(self, *args, **kwargs):
        context = super(PasswordResetCompleteView, self).get_context_data(*args, **kwargs)
        context['title'] = 'Password Reset Complete'
        return context


